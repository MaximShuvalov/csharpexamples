﻿using BasicExamples.Classes;

namespace BasicExamples
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //Пример создания экземпляра класса

            // создание экземпляра класса с конструктором без параметров
            var param1 = new Example();

            // создание экземпляра класса с конструктором с параметрами
            var param2 = new Example("testArg", "testArg2");

            //Обращения к свойству класса
            Console.WriteLine(param1.AutoProperty);
            Console.WriteLine(param2.PropertyOfOnlyGetting);

            //Присваивание значения свойству класса
            param1.AutoProperty = "new value of property";

            //Проверим, что значение свойства на самом деле поменялось
            Console.WriteLine(param1.AutoProperty);

            //Пример вызова метода класса
            param1.PrintPrivateField();

            //Так как метод Sum возвращает значение, то данное значение можно положить в переменную
            var result = param1.Sum(4, 6);
            //или передать в аргументов в какой-нибудь метод, принимающий параметр с таким же типом как и у полученного значения
            Console.WriteLine(result);

            Console.WriteLine("Нажмите 'Enter' для завершения программы");
            Console.ReadLine();
        }
    }
}

