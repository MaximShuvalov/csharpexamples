﻿using System;
namespace BasicExamples.Classes
{
	/// <summary>
	/// Пример объявления класса с именем 'Example'
	/// </summary>
	public class Example
	{
        /// <summary>
        /// Пример объявления приватного поля класса с типом 'string' и именем '_exampleField'
		/// Имена приватных полей всегда начинаются с символа '_' за которым следует название поля с маленькой буквы
        /// </summary>
        private string _exampleField;

        /// <summary>
        /// Пример объявления публичного поля класса с типом 'string' и именем 'ExampleOfPublicFieldDeclarating'
		/// публичные поля класса называются с большой буквы
        /// </summary>
        public string ExampleOfPublicFieldDeclarating;

		/// <summary>
		/// Пример объявления поля класса с послед. инициализацией
		/// </summary>
		public string ExampleOfFieldDeclaratingWithInitializing = "this is initialized field";

		/// <summary>
		/// Пример объявления автоматического свойства
		/// </summary>
		public string AutoProperty { get; set; }

		/// <summary>
		/// Пример объявления свойства только для чтения
		/// </summary>
		public string PropertyOfOnlyGetting { get; }


		/// <summary>
		/// Объявление конструктора класса, который не принимает параметров
		/// </summary>
		public Example()
		{
			_exampleField = "example private field";
			ExampleOfPublicFieldDeclarating = "example public field";
		}

		/// <summary>
		/// Объявление конструктора класса с параметрами
		/// </summary>
		/// <param name="valueOfPrivateField">Параметр 1</param>
		/// <param name="valueOfPublicField">Параметр 2</param>
		public Example(string valueOfPrivateField, string valueOfPublicField)
		{
			_exampleField = valueOfPrivateField;
			ExampleOfPublicFieldDeclarating = valueOfPublicField;
		}

		/// <summary>
		/// Пример объявления метода, который не возвращает значение и не принимает параметров
		/// </summary>
		public void PrintPrivateField()
		{
			Console.WriteLine("Hello World!");
		}

		/// <summary>
		/// Пример объявления метода, который возвращает значение типа 'int'
		/// и принимает два параметра с типом 'int' и именами 'a' и 'b'
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public int Sum(int a, int b)
		{
			return a + b;
		}
	}
}

